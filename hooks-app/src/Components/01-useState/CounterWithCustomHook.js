import React from 'react';
import{useCounter} from '../../hooks/useCounter';

import "./CounterApp.css";

const CounterWithCustomHook = () => {

    const {counter,increment,decrement,reset} = useCounter();
    return (
        <>
            <h1>Counter with Hook: {counter}</h1>
            <button onClick={ increment} className="btn btn-secondary">+1</button>
            <button onClick={ reset } className="btn btn-secondary">Reset</button>
            <button onClick={ decrement}className="btn btn-secondary">-1</button>
        </>
    )
}

export default CounterWithCustomHook;