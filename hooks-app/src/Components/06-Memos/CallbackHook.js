import React, { useCallback, useState } from 'react';
import "./Memorize.css";
import ShowIncrement from './ShowIncrement/ShowIncrement';

const CallbackHook = () => {

    const [counter, setCounter] = useState(10);


    const increment = useCallback( (num) =>{
        setCounter((count) => count + num);
    },[ setCounter ]
    )

    return (
        <div>
            <h1>CallbackHook Counter: {counter}</h1>
            <hr/>
            <ShowIncrement increment={increment}/>
        </div>
    )
}

export default CallbackHook;