
import React from 'react';
import '@testing-library/jest-dom';
import {shallow} from "enzyme";
import PrimeraApp from '../PrimeraApp';

describe('Pruebas en <PrimeraApp/>',() => {

    // test("Debe de mostrar el mensaje 'Hola,soy Ulquiorra'",() => {

    //     const hello = 'Hola,soy Ulquiorra';
    //     const {getByText} = render(<PrimeraApp salute={hello} />);

    //     expect( getByText(hello) ).toBeInTheDocument();
    // })

    test('debe de mostrar <PrimeraApp/> correctamente',() => {

        const hello = 'Hola,soy Ulquiorra';
        const wrapper = shallow(<PrimeraApp salute = {hello}/>)
        expect(wrapper).toMatchSnapshot();
    })
    test('debe de mostrar el subtitulo enviado por props',() => {

        const hello = 'Hola,soy Ulquiorra';
        const subTitulo = 'Soy un subtitulo';
        const wrapper = shallow(
        <PrimeraApp 
            salute = { hello }
            subtitulo={subTitulo}/>)
        const textoParrafo = wrapper.find('p');
        expect(textoParrafo).toMatchSnapshot();
    })

})