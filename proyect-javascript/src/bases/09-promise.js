import getHeroeById from "./08-imports";
// const promesa = new Promise((resolve, reject) => {

//     setTimeout( () => {
//         const heroe = getHeroeById(2);
//         resolve(heroe);
//     },2000)

// });

// promesa.then( (heroe) => {
//     console.log("heroe", heroe);
// })
// .catch(err => console.warning( err ));

const getHeroeByIdAsync = (id) => {
    const promesa = new Promise((resolve, reject) => {

        setTimeout( () => {
            const heroe = getHeroeById(id);
            if(heroe !== undefined){
            resolve(heroe);
       
        }else {reject("No se pudo encontrar el heroe")};
         },2000)

        });
    return promesa
}

getHeroeByIdAsync(1)
    .then (console.log)
    .catch(console.warn);
    