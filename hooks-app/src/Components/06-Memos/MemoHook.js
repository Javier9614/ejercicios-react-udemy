import React, { useMemo, useState } from 'react';
import { heightProcess } from '../../helpers/heightProcess';
import {useCounter} from '../../hooks/useCounter';
import "./Memorize.css";


const MemoHook = () => {

    const {counter, increment} = useCounter(3000);
    const [show, setShow] = useState(true);


    const memoHeightProcess = useMemo(() => heightProcess(counter), [counter])

    return (
        <div>
            <h1>MemoHook</h1>
            <h3><small>{counter}</small></h3>
            <hr/>

            <p>{memoHeightProcess}</p>

            <button 
                className="btn btn-outline-primary"
                onClick={increment}
                >
                    +1
                </button>
            <button 
                className="btn btn-outline-secondary ms-3"
                onClick={ () => {
                    setShow(!show);
                }}
                >
                    Show/Hide {JSON.stringify(show)}
                </button>
        </div>
    )
}

export default MemoHook;