import {getSaludo} from "../../base/02-template-string"

describe('Pruebas en 02-template-string.js',() => {

    test('getSaludo deberia retornar Hola Javier',() => {

        const nombre = "Javier";

        const salute = getSaludo(nombre);

        expect (salute).toBe("Hola " + nombre);

    })

    //get saludo debe de retornar Hola Carlos si no hay argumento en el nombre 
    test('getSaludo debe de retornar Hola Carlos',() => {

        const salute = getSaludo();

        expect (salute).toBe("Hola Carlos" );
    })

})