import {getUser, getUsuarioActivo} from "../../base/05-funciones";

describe('Pruebas en 05-funciones.js',()=>{

    test('getUser debe retornar un objeto',() => {

        const userTest = {
            uid: 'ABC123',
            username: 'El_Papi1502'
        }
        
        const user = getUser();
        
        //para comparar si dos objetos son iguales tenemos que usar la
        //toEqual,o toStrictEqual porque aunque los dos objetos retornan los mismos valores 
        //nunca seran iguales porque estan en distintos espacios de memoria,
        //si pruebas {}==={} saldra false por eso mismo.
        //Asi que toEqual cxomprobara que tienen los mismos campos y valores.

        expect(user).toEqual(userTest);


    })

    test("getUsuarioActivo debe retornar un objeto", () =>{ 
        const nombre = "Javi";
        const userTest = {
            uid: 'ABC567',
            username: nombre
        }

        //En este caso getUsuarioAActivo tiene un parametro
        const userActive = getUsuarioActivo(nombre);

        expect(userActive).toEqual(userTest);
    })

})