import React from 'react';
import PropTypes from"prop-types";
const PrimeraApp =({
        salute,
        subtittle
    }) =>{
    // const salute = {
    //     name:"Javi",
    //     lastName:"Aranda",
    //     age:25
    // };
    


    return (
        // <div>
        //     <h1>Hola mundo</h1>
        //     <p>Mi primera app</p>
        // </div>

        //Fragments
        <>
            <h1>{salute}</h1>
                {/* <pre>{JSON.stringify(salute, null, 3)}</pre> */}
            <p>{subtittle}</p>
        </>
    )
}

PrimeraApp.propTypes = {
    salute: PropTypes.string.isRequired,
      
}
PrimeraApp.defaultProps = {
    subtittle: "Soy un subtitulo"
}


export default PrimeraApp;