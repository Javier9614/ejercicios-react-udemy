import React from 'react';
import ReactDOM from 'react-dom';
import TodoApp from './Components/08-useReducer/TodoApp';

//import {Padre} from "../src/Components/07-tarea-memo/Padre"
//import CallbackHook from './Components/06-Memos/CallbackHook';
//import MemoHook from './Components/06-Memos/MemoHook';
//import Memorize from './Components/06-Memos/Memorize';
//import LayoutEffect from './Components/05-useLayoutEffect/LayoutEffect';
//import RealExampleRef from './Components/04-useRef/RealExampleRef';
//import FocusScreen from './Components/04-useRef/FocusScreen';
//import MultipleCustomHooks from './Components/03-examples/MultipleCustomHooks';
//import FormWithCustomHook from './Components/02-useEffect/FormWithCustomHook';
//import SimpleForm from './Components/02-useEffect/SimpleForm';
//import CounterApp from './Components/01-useState/CounterApp';
//import CounterWithCustomHook from './Components/01-useState/CounterWithCustomHook';
//import HookApp from './HookApp';


ReactDOM.render(
  <TodoApp/>,
  document.getElementById('root')
);

