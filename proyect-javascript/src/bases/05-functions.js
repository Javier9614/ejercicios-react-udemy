


const salute = ((name) => {
    return `hola, ${name}`;
})

const salute2 = (name) => {
    return `hola, ${name}`;
}

const salute3 = (name) => `hola, ${name}`;


const salute4 = () => `hola munndo`;


console.log(salute("Goku"));
console.log(salute2("Vegeta"));
console.log(salute3("Gohan"));
console.log(salute4("Piccolo"));


const getUser = () => {
    return {
        uid: "ABC123",
        username:"ArmadillitoFelih"
    }
}

const user = getUser();
console.log(user);

//retornamos un objeto implicito
const getUserActive = (name) =>( 
     {
        uid:"ABC567",
        username: name
    });


const userActive = getUserActive("Javier");
console.log(userActive);

