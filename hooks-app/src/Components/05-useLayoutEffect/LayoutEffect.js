import React, { useLayoutEffect, useRef } from "react";
import { useFetch } from "../../hooks/useFetch";
import { useCounter } from "../../hooks/useCounter";
import "./LayoutEffect.css";

const LayoutEffect = () => {
  const { counter, increment } = useCounter(1);

  const {  data } = useFetch(
    `https://www.breakingbadapi.com/api/quotes/${counter}`
  );

  const {  quote } = !!data && data[0]; //si existe la data estrae la pos 0

    const parRef = useRef();

    useLayoutEffect(() => {
        
        console.log(parRef.current.getBoundingClientRect());

    },[quote])

  return (
    <div>
      <h1>useLayoutEffect</h1>
      <hr />

      <blockquote className="blockquote  text-end">
        <p className="mb-2" ref={parRef}>{quote}</p>
        
      </blockquote>

      <button className="btn btn-primary" onClick={increment}>
        Siguente quote
      </button>
    </div>
  );
};

export default LayoutEffect;
