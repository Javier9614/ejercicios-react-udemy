const person = {
    name : "Tony",
    lastName: "Stark",
    age : 45,
    street:{
        city : "New York",
        zip: 98897,
        lat: 14.3232,
        lng:34.1212,
    }
};

//console.log({person:person});
//Es lo mismo
//console.log({person});

//Te lo muestra en forma de tabla, interesante
//console.table(person);

console.log(person);

const person2 = {...person};

person2.name = "Peter";
console.log(person);
console.log(person2);


