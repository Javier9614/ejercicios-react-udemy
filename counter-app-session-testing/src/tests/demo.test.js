//Para ver las pruebas, abriremos una nueva terminal en la carpeta del proyecto
 //y inicaremos el comando npm run test, que ejecutara automaticamente todos los tests
 //que tengamos.

 
 //Esto es opcional, pero aqui se verian todas la pruebas que hacemos de manera ordenada
 //en nuestra consola 
 
 describe("Pruebas en el archivo demo.test.js", () => {
     
     //La funcion test se encarga de hacer funcionar el comando de antes
     
     test ( 'Deben ser iguales los strings', () =>{ 
         // 1. Inicializacion
         const message = "Hola mundo";
     
         //2. Estimulo
         const message2 = `Hola mundo`;
     
         //3. Observar el comportamiento
         //Aqui comparamos que los dos strings sean iguales
         expect(message).toBe(message2);
     
     } );
     
});
