const person = {
    name: "Tony",
    age: 45,
    key: "IronMan"
};

const {name, age, key } = person;

console.log(name);
console.log(age);
console.log(key);

const printPerson = ({ key, name, range="S Tier"}) => {
    console.log(key, name, range );
    return { 
        keyName: key,
        years:age,
        latlng:{
            lat:14.545551,
            lng:-54.781231
        }
    }
      
}

const avenger = printPerson(person);
//desestructurar objetos de objetos
const {keyName, years, latlng} = avenger;
const {lat,lng} = latlng;
console.log(keyName,years);
console.log(lat,lng);