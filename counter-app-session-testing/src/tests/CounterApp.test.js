import { shallow } from "enzyme";
import CounterApp from "../CounterApp";

describe('Pruebas en <CounterApp/>',() => {

    let wrapper = shallow(<CounterApp />);

    beforeEach(() => {
    wrapper = shallow(<CounterApp />);

    })

    test('Debe mostrar  <CounterApp/> correctamente', () => {

        expect(wrapper).toMatchSnapshot();

    })
    test('Debe mostrar el valor del contador enviado por props', () => {

        
        const wrapper = shallow(<CounterApp value={10}/>);
        const valueOfCounter = wrapper.find('h2').text().trim();
        expect(valueOfCounter).toBe("10");

    })

    test('Debe incrementar +1 al pulsar el boton',() => {

        wrapper.find('button').at(0).simulate('click');

        //console.log(btn1.html());
        const valueOfCounter = wrapper.find('h2').text().trim();
        expect(valueOfCounter).toBe("11");
    })
    
    test('Debe restar -1 al pulsar el boton',() => {
        
        wrapper.find('button').at(2).simulate('click');
        
        
        const valueOfCounter = wrapper.find('h2').text().trim();
        expect(valueOfCounter).toBe("9");
    })
    test('Debe restaurar el valor inicial al pulsar el boton',() => {
    
        const wrapper = shallow(<CounterApp value={105}/>);
        
        wrapper.find('button').at(2).simulate('click');
        wrapper.find('button').at(1).simulate('click');
    
        
        const valueOfCounter = wrapper.find('h2').text().trim();
        expect(valueOfCounter).toBe("105");
    })
})